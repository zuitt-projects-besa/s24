/*1 - Find users with y in first and last name (Show email and isAdmin properties)*/
	db.users.find({
	$or:[
			{"firstName" : {$regex: 'y', $options: '$i'}},
			{"lastName" : {$regex: 'y', $options: '$i'}}
		],
	},
	{"_id" : 0, "email":1 , "isAdmin": 1})

/*2 - Find users with e and isAdmin (Show email and isAdmin properties)*/
	db.users.find({
	$and:[
			{"firstName" : {$regex: 'e', $options: '$i'}},
			{"isAdmin" : true}
		],
	},
	{"_id" : 0, "email":1 , "isAdmin": 1})

/*3 - Find products with letter x and price >= 50000*/
	db.products.find({
	$and:[
			{"name" : {$regex: 'x', $options: '$i'}},
			{"price": {$gte : 50000}}
		],
	})

/*4 - Update Price < 2000*/
	db.products.updateMany({"price": {$lt : 2000}}, {$set: {"isActive" : false}});

/*5 - Delete Price > 20000*/
	db.products.deleteMany({"price": {$gt : 20000}});